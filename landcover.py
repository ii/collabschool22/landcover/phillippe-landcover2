#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Day 2 activity with numpy and scipy in the summer school

import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt

from math import ceil
from matplotlib import colors

def plot_map(data, default_colors=False):
    if default_colors:
        plt.imshow(data)
        plt.show()
    else:
        # optionally setting suggesting colors
        maryland_colors = np.array([( 68,  79, 137),
                                    (  1, 100,   0),
                                    (  1, 130,   0),
                                    (151, 191,  71),
                                    (  2, 220,   0),
                                    (  0, 255,   0),
                                    (146, 174,  47),
                                    (220, 206,   0),
                                    (255, 173,   0),
                                    (255, 251, 195),
                                    (140,  72,   9),
                                    (247, 165, 255),
                                    (255, 199, 174),
                                    (  0, 255, 255),]) / 255
        maryland_cmap = colors.ListedColormap(maryland_colors)
        plt.imshow(data, cmap=maryland_cmap)  ## plotting a subset of points (stepsize50)
        plt.show()


# takes north-south coordinate (degrees in [-90, 90]) and returns corresponding row in the input data
def latitude_coordinate_to_row_idx(coord):
    coord += 90   # now in [0,180]
t    row = math.ceil(coord / (1./120.))   # 0.00833 = 1/120 increments in the data
    if row == 0:
        return 0
    else
        return row - 1   # -1 because data is in [0, 21599]


# takes west-east coordinate (degrees in [-180, 180]) and returns corresponding column in the input data
def longitude_coordinate_to_column_idx(coord):
    coord += 180   # now in [0,360]
t    col = math.ceil(coord / (1./120.))   # 0.00833 = 1/120 increments in the data
    if col == 0:
        return 0
    else
        return col - 1   # -1 because data is in [0, 43199]

# in the original data, 0 is water and land types are in [1,...13]
def is_this_on_land(data, lat, lon):
    row = latitude_coordinate_to_row_idx(lat)
    col = longitude_coordinate_to_column_idx(lon)
    return data[row][col] > 0

FNAME = "/opt/tmp/gl-latlong-1km-landcover.bsq"
count = 21600*43200   # 43200 Pixels 21600 Lines

original_data = np.fromfile(FNAME, dtype=np.uint8, count=count)
original_data = original_data.reshape(21600,43200)

# uncomment to plot a subset of points (stepsize 50)
#plot_map(original_data[::50,::50])

